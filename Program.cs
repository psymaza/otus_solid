﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace OtusSolid
{
    class Program
    {
        static void Main(string[] args)
        {
            var settings = new SettingsGetter();
            var reader = new AnswerReader();
            var player = new Player(settings, reader);

            var results = player.Play(new GuessTheNumberManyTries());
            var writer = new AnswerWriter();
            writer.Write(results);
        }
    }
}
