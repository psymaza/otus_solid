﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public class GameResults:IGameResults
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
