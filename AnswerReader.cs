﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public class AnswerReader:IAnswerReader
    {
        public int Read(int tryNumber)
        {
            Console.WriteLine($"Угадайте число: попытка {tryNumber + 1}");
            var strAnswer = Console.ReadLine();
            if (int.TryParse(strAnswer, out int answer))
                return answer;
            else
                throw new ArgumentException($"Не получилось перевести в int значение: {strAnswer}");
        }

    }
}
