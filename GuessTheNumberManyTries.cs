﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public class GuessTheNumberManyTries : GuessTheNumber
    {
        public override IGameResults Play(ISettingsGetter settingsGetter, IAnswerReader answerReader)
        {
            var rand = new Random();
            var number = rand.Next(settingsGetter.MinValue, settingsGetter.MaxValue);
            bool isRightAnswer = false;
            for (int i = 0; i < settingsGetter.NumberOfTries; i++)
            {
                var answer = answerReader.Read(i);
                if (answer == number)
                {
                    isRightAnswer = true;
                    break;
                }

            }
            var gameResults = new GameResults();
            if (isRightAnswer)
            {
                gameResults.IsSuccess = true;
            }
            else
            {
                gameResults.IsSuccess = false;
                gameResults.Message = $"Попытки кончились, правильное число {number}";
            }
            return gameResults;
        }
    }
}
