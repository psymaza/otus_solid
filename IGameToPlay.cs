﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public interface IGameToPlay
    {
        IGameResults Play(ISettingsGetter settingsGetter, IAnswerReader answerReader);
    }
}
