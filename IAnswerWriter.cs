﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public interface IAnswerWriter
    {
        public void Write(IGameResults gameResults);
    }
}
