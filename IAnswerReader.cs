﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public interface IAnswerReader
    {
        int Read(int tryNumber);
    }
}
