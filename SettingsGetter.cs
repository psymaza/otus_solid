﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OtusSolid
{
    public class SettingsGetter:ISettingsGetter
    {
        public int MinValue { get; private set; }
        public int MaxValue { get; private set; }
        public int NumberOfTries { get; private set; }
        public SettingsGetter()
        {
            var builder = new ConfigurationBuilder()
.SetBasePath(Directory.GetCurrentDirectory())
.AddJsonFile("appsettings.json");
            var configuration = builder.Build();
            if (!int.TryParse(configuration["minValue"], out int minValue))
            {
                Console.WriteLine($"Указан некорректный параметр в конфиг файле appsettings.json {configuration["minValue"]}");
            }
            MinValue = minValue;
            if (!int.TryParse(configuration["maxValue"], out int maxValue))
            {
                Console.WriteLine($"Указан некорректный параметр в конфиг файле appsettings.json {configuration["maxValue"]}");
            }
            MaxValue = maxValue;
            if (!int.TryParse(configuration["maxTries"], out int maxTries))
            {
                Console.WriteLine($"Указан некорректный параметр в конфиг файле appsettings.json {configuration["maxTries"]}");
            }
            NumberOfTries = maxTries;
        }
    }


}
