﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public class GuessTheNumber : IGameToPlay
    {
        public virtual IGameResults Play(ISettingsGetter settingsGetter, IAnswerReader answerReader)
        {
            var rand = new Random();
            var number = rand.Next(settingsGetter.MinValue, settingsGetter.MaxValue);
            bool isRightAnswer = false;

            var answer = answerReader.Read(0);
            if (answer == number)
            {
                isRightAnswer = true;
            }

            var gameResults = new GameResults();
            if (isRightAnswer)
            {
                gameResults.IsSuccess = true;
            }
            else
            {
                gameResults.IsSuccess = false;
                gameResults.Message = $"Попытки кончились, правильное число {number}";
            }
            return gameResults;
        }
    }
}
