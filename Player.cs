﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public class Player
    {
        ISettingsGetter _settingsGetter;
        IAnswerReader _answerReader;

        public Player(ISettingsGetter settingsGetter, IAnswerReader answerReader)
        {
            _settingsGetter = settingsGetter;
            _answerReader = answerReader;
        }

        public IGameResults Play(IGameToPlay gameToPlay)
        {
            return gameToPlay.Play(_settingsGetter, _answerReader);
        }
    }
}
