﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public interface ISettingsGetter
    {
        public int MinValue { get;  }
        public int MaxValue { get; }
        public int NumberOfTries { get; }
    }
}
