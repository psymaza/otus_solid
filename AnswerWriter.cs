﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtusSolid
{
    public class AnswerWriter: IAnswerWriter
    {
        public void Write(IGameResults gameResults)
        {
            if (gameResults.IsSuccess)
                Console.WriteLine("Урра! Вы угадали");
            else
            {
                Console.WriteLine("Игра окончена");
                Console.WriteLine(gameResults.Message);
            }
        }
    }
}
